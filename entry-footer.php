<footer class="entry-meta">	
	<?php
	printf( __( '<img src="https://blog.translatescience.org/wp-content/themes/SemPress-child/includes/attribution-box/by.svg">', 'sempresschild' ));

	printf( __( 'Except where otherwise noted, the content on this site is licensed under a <a href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International</a> License. ', 'sempresschild' ) ); 	

	if ( in_array( get_post_format(), array( 'aside', 'link', 'status', 'quote' ) ) || ! get_the_title()  ) {
		sempress_posted_on();
	} else {
		_e( 'Posted', 'sempress' );
	}
	?>
	<?php
	/* translators: used between list items, there is a space after the comma */
	$categories_list = get_the_category_list( __( ', ', 'sempress' ) );
	if ( $categories_list ) :
	?>
	<span class="cat-links">
		<?php printf( __( 'in %1$s', 'sempress' ), $categories_list ); ?>
	</span>
	<?php endif; // End if categories ?>

	<?php
	/* translators: used between list items, there is a space after the comma */
	$tags_list = get_the_tag_list( '', __( ', ', 'sempress' ) );
	if ( $tags_list ) :
	?>
	<span class="sep"> | </span>
	<span class="tag-links" itemprop="keywords">
		<?php printf( __( 'Tagged %1$s', 'sempress' ), $tags_list ); ?>
	</span>
	<?php endif; // End if $tags_list ?>

	<?php do_action( 'sempress_entry_footer' ); ?>

	<?php if ( comments_open() || ( '0' != get_comments_number() && ! comments_open() ) ) : ?>
	<span class="sep"> | </span>
	<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'sempress' ), __( '1 Comment', 'sempress' ), __( '% Comments', 'sempress' ) ); ?></span>
	<?php endif; ?>

	<?php edit_post_link( __( 'Edit', 'sempress' ), '<span class="sep"> | </span><span class="edit-link">', '</span>' ); ?>
</footer><!-- #entry-meta -->
