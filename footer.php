<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package SemPress
 * @since SemPress 1.0.0
 */
?>

	</div><!-- #main -->

	<footer id="colophon" role="contentinfo">
		<div id="site-publisher" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
			<meta itemprop="name" content="<?php echo get_bloginfo( 'name', 'display' ); ?>" />
			<meta itemprop="url" content="<?php echo esc_url( home_url( '/' ) ); ?>" />
			<?php
			if ( has_custom_logo() ) {
				$image = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ) );
			?>
				<div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
					<meta itemprop="url" content="<?php echo current( $image ); ?>" />
					<meta itemprop="width" content="<?php echo next( $image ); ?>" />
					<meta itemprop="height" content="<?php echo next( $image ); ?>" />
				</div>
			<?php } ?>
		</div>

		<div class="site-info">
			<ul>
				<?php
					if ( stripos(home_url(), "blog.") ) { 						
						echo '<li class="site-title"><a href="' . esc_url( home_url( '/' ) ) . '" rel="home">' . bloginfo( 'name' ) . '</a></li>';
						echo '<li class="main-site-title">';
						do_action( 'sempress_credits' );
						printf( __( 'The blog of %1$s.', 'sempress-child' ), '<a href="https://TranslateScience.org/">Translate Science</a>' );
						echo '</li>';
					}				
				?>
				<li><a href="https://translatescience.org/privacy-policy/">Privacy policy</a></li>
				<li><a href="<?php echo esc_url( home_url( '/wp-admin/' ) ); ?>">Site Admin</a> (for members)</li>
			</ul>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
